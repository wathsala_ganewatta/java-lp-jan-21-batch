import java.lang.Exception;
import java.util.Scanner;
import java.util.InputMismatchException;

class ExceptionHandling{

	public void transaction(Account account,Scanner scanner){
		System.out.println("Select option:\n\t 1.withdraw\n\t 2.deposite \n\t 3.Check balance\n\t 4.Exit");
		int option= scanner.nextInt();
		int amount ;
		switch(option)
		{
			case 1:System.out.println("Enter amount to withdraw :");
				try{
					amount = scanner.nextInt();			
					account.withdraw(amount);
					System.out.println("You have Successfully withdraw "+amount);
					System.out.println("Your Courrent balance is :" + account.getBalance()+"\n\n");
					System.out.println("Continue?(y/n)");					
					char x = scanner.next().charAt(0);
					if(x=='y')
						transaction(account,scanner);
					else if(x == 'n')
						break;
		
							
						
				}
				catch(InputMismatchException ex){
					System.out.print("you didn't enter a number value ");
				}
				catch(InsufficientFundException ex)
				{
					ex.printStackTrace();	
				}
				break;
			case 2:System.out.println("Enter amount to deposite:");
				try{
					amount = scanner.nextInt();
					account.deposite(amount);
					System.out.println("You have Successfully deposite "+amount);
					System.out.println("Your Courrent balance is :" + account.getBalance()+"\n\n");
					System.out.println("Continue?(y/n)");					
					char x = scanner.next().charAt(0);
					if(x=='y')
						transaction(account,scanner);
					else if(x == 'n')
						break;
				
				}
				catch(InputMismatchException ex){
						System.out.print("you didn't enter a number value ");
				}
				catch(AmountNotValidException  ex)
				{
					ex.printStackTrace();	
				}
				break;
			case 3:System.out.println("Your Courrent balance is :" + account.getBalance()+"\n\n");
				transaction(account,scanner);
				break;
			case 4:System.out.println("Thank you for Connecting with the java banking system");
				break;
			default:transaction(account,scanner);
			

		}

		
	}
	public static void main(String[] args){
		ExceptionHandling exceptionHandling = new ExceptionHandling();
		Account account = new Account();
		Scanner scanner = new Scanner(System.in);
		System.out.println("==========If you enter invalid id number or wrong id number it gives the four level of stackTrace=========");
		System.out.println("===If you put a correct id number and withdraw money higher than the balance it gives a runtime Exception(Unchecked Exception)===");
		System.out.println("Your Courrent Account balance is "+ account.getBalance());
		System.out.println("Your id number is 789456152v");
		System.out.println("Enter your id ");
		String id = scanner.next();
		

		System.out.println("\n\n2StackTrace\n");

		try{
			account.find(id);
			exceptionHandling.transaction(account,scanner);
		}
		catch( TransactionNotValidException ex){
			ex.printStackTrace();
		}
		

		

		

	}
}