public class TransactionNotValidException extends Exception{

	public TransactionNotValidException(String message, Throwable cause)
	{
		super(message,cause);
	}	
}