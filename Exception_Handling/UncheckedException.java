import java.util.Scanner;
public class UncheckedException{
	public static void main(String[] args){
		
		Account account = new Account();
		System.out.println("Account Balance is "+ account.getBalance());
		System.out.println("Enter amount to withdraw");
		Scanner scanner = new Scanner(System.in);
		int amount = scanner.nextInt(); 
		try{
			account.withdraw(amount);
		}
		catch(InsufficientFundException ex)
		{
			ex.printStackTrace();
		}
	}
}