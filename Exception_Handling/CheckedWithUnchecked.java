import java.util.Scanner;
public class CheckedWithUnchecked{

	public static void main(String[] args){
	Scanner scanner = new Scanner(System.in);
	//CheckedWithUnchecked checkedWithUnchecked = new CheckedWithUnchecked();
	Account account = new Account();
	System.out.println("Enter your id ");	
	String id = scanner.next();
		

	try{
			
		if(!(id.trim().matches("^[0-9]{9}[vVxX]$")))
		{
			throw new IDNotMatchWithRegExException("regEx not match");
		}
			
	}
	catch(IDNotMatchWithRegExException ex){
		throw new IdNotValidException (ex);
	}
}
}