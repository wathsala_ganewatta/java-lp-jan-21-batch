
public class Account{

	private int balance = 3000;

	public int getBalance(){
		return balance;
	}

	public void  deposite(int amount) {
		
			if(amount>0){
				
				balance = balance + amount;
			}
			else{
				throw new AmountNotValidException ("Amount not valid to deposite :: RuntimeException Handling");
			}
		
		
	}

	public void find(String id) throws TransactionNotValidException  {
		try{
			
			accountMacthing(id);
			
						
		}
		catch(AccountNotFoundException  ex)	{
			throw new TransactionNotValidException("Transaction not valid" , ex);
		}
		
	}
	public void withdraw(int amount) {
		if(amount>balance){
				throw new InsufficientFundException ("Insufficient balance for withdraw :: RuntimeException Handling");
		}
		balance-=amount;

	}



	public void accountMacthing(String id) throws AccountNotFoundException  {
		try{
			idValidation(id);
			
		}
		catch( IdNotValidException ex){
			throw new AccountNotFoundException ("Account not found",ex);
		}
	
	}

	public void findId(String id) throws IDNotFoundException{

		if((id.equals("789456152v"))){
			System.out.print("Id is match");
		}
		else {
			throw new IDNotFoundException("Id not Found");
		}
	}

	public void idValidation(String id) throws IdNotValidException {
		
		try
		{
			if(!(id.trim().matches("^[0-9]{9}[vVxX]$")))
			{
				throw new IDNotMatchWithRegExException("regEx not match");
			}
			findId(id);
		}
		catch(IDNotMatchWithRegExException ex)
		{
			throw new IdNotValidException ("id not valid",ex);
		}
		catch(IDNotFoundException ex)
		{
			//throw new IdNotValidException ("id not valid" ,ex);
		}
			
	}

}
