import java.util.Scanner;
class ReverseNumber{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter number to reverse : ");
		int number = scanner.nextInt();
		int reverseNumber = 0;
		while(number !=0)
		{
			reverseNumber = (reverseNumber*10) + number%10;
			number = number/10;
		}
		System.out.print("Reversing number is "+reverseNumber);
	}
		

}